# Galgame scoop bucket

## List

| name                                                            | id                              |
| -----------------------------------                             | ------------------------------  |
| 亚托莉 - 我挚爱的时光                                           | ATRI-MyDearMoments-             |
| 星辰恋曲的白色永恒-Finale-                                      | AstralAirFinale                 |
| BLOODY RONDO                                                    | BLOODYRONDO                     |
| Butterfly Seeker                                                | ButterflySeeker                 |
| CHAOSCHILD                                                      | CHAOSCHILD                      |
| 星光咖啡馆与死神之蝶                                            | CafeStella                      |
| 青空下的加缪                                                    | CamusInTheBlueSky               |
| ChaosHead Love Chu Chu                                          | ChaosHeadLoveChuChu             |
| Chaos;Head NOAH                                                 | ChaosHeadNoah                   |
| 大帝国                                                          | Daiteikoku                      |
| 与我恋爱的废柴小恶魔                                            | DitzyDemons                     |
| Doki Doki Literature Club Plus                                  | Doki_Doki_Literature_Club_Plus  |
| Empty×Embryo-E×E                                                | EmptyXEmbryo                    |
| EndlessDungeon                                                  | EndlessDungeon                  |
| 夏娃年代记 2                                                    | Evenicle2                       |
| FLOWERS 秋篇                                                    | FLOWERS_autumn                  |
| Flyable CandyHeart                                              | FlyableCandyHeart               |
| Fortissimo FA                                                   | Fortissimo_FA                   |
| 抬头看看吧，看那天上的繁星                                      | FullOfStar                      |
| 灰色幻影扳机 Vol.1                                              | GrisaiaPhantomTrigger1          |
| 灰色幻影扳机 Vol.2                                              | GrisaiaPhantomTrigger2          |
| 白衣性愛情依存症                                                | Hakuai                          |
| 向日葵 Aqua After                                               | HimawariAquaAfter               |
| 向日葵：天空鹅卵石                                              | HimawariPebbleintheSky          |
| ISLAND                                                          | ISLAND                          |
| KARAKARA                                                        | KARAKARA                        |
| KARAKARA2                                                       | KARAKARA2                       |
| 与她＊心渐近                                                    | KanojoStep                      |
| LAMUNATION！                                                    | LAMUNATION                      |
| 时钟机关的 Ley-line－残影之夜将明时                             | Leyline2                        |
| 时钟机关的 Ley-line－朝雾中飘零之花                             | Leyline3                        |
| Making＊Lovers                                                  | MakingLovers                    |
| Making＊Lovers FD1                                              | MakingLoversFD1                 |
| Making＊Lovers FD2                                              | MakingLoversFD2                 |
| MeltyMoment                                                     | MeltyMoment                     |
| MinDeaD                                                         | MinDeaD                         |
| Rewrite+                                                        | Rewrite+                        |
| RIDDLE JOKER                                                    | RiddleJoker                     |
| STARLESS                                                        | STARLESS                        |
| 命运石之门 0                                                    | STEINSGATE0                     |
| 千之刃涛，桃花染之皇姬 -柳暗花明 -                              | SenmomoFD                       |
| 千恋＊万花                                                      | SenrenBanka                     |
| 少女＊领域                                                      | shoujoryouki                    |
| Summer Pockets                                                  | SummerPockets                   |
| 対魔忍阿莎姬 2～淫谋之东京王国～完全版                          | TaimaninAsagi                   |
| VenusBlood                                                      | VenusBlood                      |
| Wind -a breath of heart-                                        | WindRP                          |
| 流星·世界演绎者                                                 | WorldActor                      |
| 爱之钥                                                          | aikagi                          |
| 爱之钥 2                                                        | aikagi2                         |
| 爱之钥 ~AfterDays~                                              | aikagi_afterdays                |
| 雨音 Switch ~我与生病的她以及不停歇的雨~                        | amane                           |
| 青鸟                                                            | aoitori                         |
| 苍蓝彼端的四重奏 完备版                                         | aokana                          |
| 朝霞之歌                                                        | asayake                         |
| 菖蒲的城镇与公主                                                | ayameno                         |
| 大番长                                                          | bancho                          |
| 少女与野兽                                                      | beast                           |
| 美少女万华镜 -罪与罚的少女 -                                    | biman4                          |
| 我只注视着你                                                    | bokukimi                        |
| 我的一人战争                                                    | bokusen                         |
| BUNNYBLACK 3                                                    | bunny3                          |
| 管乐恋曲！                                                      | buraban                         |
| 萝莉吸血鬼！～与吸血鬼姐妹的性福生活～                          | chibipire                       |
| 駄作 ～ヌイアワセ～                                             | dasaku                          |
| Dear Sister～真由是只属于哥哥的东西哦！！                       | dearsis                         |
| 斩魔大圣                                                        | demonbane                       |
| 多娜多娜 一起来干坏事吧                                         | Dohnadohna                      |
| 同级生 Original 版                                              | dokyu                           |
| 英雄＊戦姫 GOLD                                                 | eiyusenkiG                      |
| 海市蜃楼之馆                                                    | fatamorgana                     |
| 花之天使的夏日恋歌                                              | floflo                          |
| 学校的圣域                                                      | gakkou_seiiki                   |
| 箱庭的学园                                                      | hakoniwa                        |
| 花咲 work spring                                                | hanasaki                        |
| 幸福噩梦                                                        | hapymaher                       |
| 清澄如鏡之水面上                                                | harumina                        |
| 春天的足音                                                      | haruoto                         |
| 千之刃涛，桃花染之皇姬                                          | hatou                           |
| 秘蜜淫交地下俱樂部～沉溺於媚藥的肉體～                          | himitsuinko_chikaclub           |
| 秘密基地里的羞羞事                                              | himitu                          |
| 星恋＊twinkle                                                   | hoshikoi                        |
| 家有仙妻～无名小猫又与神 Tuber～                                | iegami2                         |
| 家有神妻 ~恋上金发兔因幡~                                       | iegami3                         |
| 妹之形                                                          | imokata                         |
| 与妹妹的记忆                                                    | imokega                         |
| 女装学園（孕）                                                  | jogaku                          |
| 纸上魔法使                                                      | kamimaho                        |
| 求神太多我的未来糟糕了                                          | kamiyaba                        |
| 永不枯萎的世界与终结之花                                        | kareseka                        |
| 寄宿之恋                                                        | kari                            |
| 卡卢玛卢卡＊同好会                                              | karumaruka                      |
| 图谋不轨的恋爱                                                  | kimilove                        |
| 金色 Loveriche                                                  | kinkoi                          |
| 想要传达给你的爱恋                                              | koikake                         |
| 初恋症候群                                                      | koishiro                        |
| 在这片天空下展开一双翅膀                                        | konosora                        |
| 离开的人们                                                      | kyojintachi                     |
| 与小萝莉相思相爱                                                | lol                             |
| 同居恋人洛丽塔                                                  | lol3                            |
| 双子洛丽塔后宫                                                  | lol4                            |
| 爱欲姐妹洛丽塔                                                  | lol5                            |
| 与小萝莉相思相爱的生活                                          | lolife                          |
| 爱上火车-Last Run!!                                             | maitetsu                        |
| 妹嫁 ～妹妹新娘～                                               | maiyome                         |
| 魔女恋爱日记                                                    | majokoi                         |
| 波与波之间～细波诊疗所～外传 裕美子的新婚日记                   | manifan                         |
| 波与波之间～细波诊疗所～                                        | manimani                        |
| 迷途的二人与世界的全部                                          | mayosube                        |
| 迷途的二人与世界的全部 LOVE HEAVEN 300％                        | mayosubeFD                      |
| 冥色の隷姫                                                      | meisyokunoreiki                 |
| 牵绊闪耀的恋之伊吕波                                            | mekuiro                         |
| 与抖 M 精灵的异世界靡孕生活                                     | melf                            |
| 凉风的物语                                                      | melt                            |
| 翠之海                                                          | midorinoumi                     |
| 与发情高冷 M 会长的过激甜蜜学园生活                             | mkaityo                         |
| 无限炼奸 ~不死舞姬的凌辱轮舞曲~                                 | mugen                           |
| 秘密的秘密                                                      | nainai                          |
| 说谎的娜蕾特与柔情的暗杀者                                      | nallet                          |
| 夏梦渚                                                          | natsuyume                       |
| NEKO-NIN exHeart                                                | nekonin                         |
| NEKO-NIN exHeart 2                                              | nekonin2                        |
| NEKO-NIN exHeart +PLUS Nachi                                    | nekonin_nachi                   |
| NEKO-NIN exHeart +PLUS Saiha                                    | nekonin_saiha                   |
| NEKOPARA Extra                                                  | nekopara_extra                  |
| 9-nine-春色春恋春熙风                                           | nine_haruiro                    |
| 9-nine-九次九日九重色                                           | nine_kokoiro                    |
| 9-nine-天色天歌天籁音                                           | nine_sorairo                    |
| 野良与皇女与流浪猫之心                                          | nora                            |
| 野良与皇女与流浪猫之心 2                                        | nora2                           |
| 野良与皇女与流浪猫之心 VFB                                      | nora_VFB                        |
| 哥哥，早上起床之前都要抱紧我哦！                                | onigyu                          |
| 哥哥，早上起床之前都要抱紧我哦！晚上睡觉之前做更多色色的事吧！  | onigyuh                         |
| 托了未来孩子的福，我娶了五个老婆                                | oreyome5                        |
| 少女理论及其之后的周边                                          | otome_sonogo                    |
| 母娘乱館                                                        | oyakorankan                     |
| ぷにろり湯 · つう                                               | puniyu2                         |
| 乐园～一如既往的我。的存在～                                    | rakuen                          |
| 与你相恋的恋爱 Recette                                          | recette                         |
| 罪之光 Rendezvous                                               | rendezvous                      |
| 轮舞曲 Duo：夜明的最强音                                        | rondoduo                        |
| 樱舞落花恋模样                                                  | sakukoi                         |
| 樱之杜 净梦者                                                   | sakuranomori                    |
| 杀恋 ～悠久流传的恋之歌～                                       | satsukoi                        |
| 世界与世界的中心                                                | sekachu                         |
| 雪影 -setsuei-                                                  | setsuei                         |
| 紫电～环之羁绊～                                                | shiden                          |
| 潮风消失之海                                                    | siokaze                         |
| 创刻的动脉                                                      | soukokunoarterial               |
| 生命的备件                                                      | spare                           |
| 宿星 Girlfriend                                                 | syukugar                        |
| 宿星 Girlfriend2                                                | syukugar2                       |
| 宿星 Girlfriend3                                                | syukugar3                       |
| 游魂 2 -you're the only one-                                    | tayutama2                       |
| 游魂 It’s happy days                                            | tayutamaFD                      |
| 被黑暗洗礼的天使                                                | teakamamire                     |
| 共结情缘的剑舞恋曲 -椿恋歌 -                                    | trenka                          |
| Trinoline                                                       | trinoline                       |
| 月影的幻影 解放之羽                                             | tuki2                           |
| 凭夜村                                                          | tukiyo                          |
| 娇蛮任性 HIGHSPEC                                               | wagahigh                        |
| 少女骑士物语 More＆More                                         | walromamore                     |
| your diary +H                                                   | yourdiary+H                     |
| 行尸走肉横行的世界只有我不被袭击 vol.0                          | zombie0                         |
| 末世孤雄 2                                                      | zombie2                         |
| 末世孤雄 3                                                      | zombie3                         |
| 灵感满溢的甜蜜创想                                              | hamidashi                       |
| 异世界酒馆六重奏 1                                              | fts1                            |
| 异世界酒馆六重奏 2                                              | fts2                            |
| 异世界酒馆六重奏 3                                              | fts3                            |
| PARQUET                                                         | parquet                         |
| Perfect Gold                                                    | perfect_gold                    |
| Salthe                                                          | salthe                          |
| 幽世灾厄现世战姬 ～沙耶香篇～                                   | kakuriyo1                       |
| Summer Pockets REFLECTION BLUE                                  | summer_pockets_reflection_blue  |
| 7days—与你共度的七日间                                          | seven_days                      |
| 可爱女友的获取方法                                              | sutekano                        |
| 回家之前的棉花糖                                                | mashimaro                       |
| 在月之彼岸邂逅                                                  | tsukikana                       |
| 天文钟里的亚莉亚                                                | orloi_aria                      |
| 天降虚拟偶像                                                    | denshi_otome                    |
| 此花绽放季，与君重逢时                                          | kimihana                        |
| 瑠璃櫻                                                          | ruizakura                       |
| 甜蜜女友 2                                                      | amakano2                        |
| fault milestone one                                             | faultms1                        |
| Dungeons&Dolls                                                  | dungeons_dolls                  |
| ZERO                                                            | zero                            |
| 濒死轮回的卿于馆中萌生的憎恶                                    | nikuniku                        |
| 妹妹和朋友与我的日常                                            | imoyaba                         |
| FLOWERS 春篇                                                    | FLOWERS_spring                  |
| 思影                                                            | omokage                         |
| 与冒牌碧池的恋人养成之路                                        | norn_bitch                      |
| 深层平行悖论 -平行世界里的理想乡 -                              | paradox                         |
| 爱上父亲的「彩子」 ～我的爸爸谁也不给～                         | chichikoi                       |
| 与你拉钩起誓的婚约                                              | lol2                            |
| 架向星空之桥 AA                                                 | hoshi_aa                        |
| 少女编织爱的画布                                                | otomega                         |
| 我的娇弱女友                                                    | itakano                         |
| Out Vegetables（怪盗夜祭）                                      | kaitou                          |
| 片轮少女                                                        | katawashoujo                    |
| 她和我与恋人                                                    | tototo                          |
| 梦与现实的联理结                                                | matryoshka                      |
| Clover Day’s Plus                                               | cloverdaysplus                  |
| 续・杀戮之姜戈 -地狱通缉犯 -                                    | django                          |
| 真恋姬无双                                                      | shinkoihime                     |
| 命运石之门：线形拘束的表征图                                    | sgfd2                           |
| 逃避行 GAME                                                     | touhikou                        |
| CAFE SOURIRE                                                    | cafesourire                     |
| 窥视名玩                                                        | nozokimibaibu                   |
| 任性妹！                                                        | namaiki                         |
| 王贼                                                            | ohzoku                          |
| 关于唯一能让笨蛋妹妹变聪明的方法只有我的 ×× 的事                | bakaimo                         |
| 天气雨                                                          | tenkiame                        |
| 只用我的 ×× 就能治好妹妹的中二病和家里蹲吗？                    | pitafeti                        |
| 夏娃年代记（Evenicle）外典                                      | EvenicleApc                     |
| 铳骑士 Cutie☆Bullet                                             | CutieBullet                     |
| 黑曜镜的魔兽                                                    | kokuyou                         |
| 夏娃年代记（Evenicle）                                          | Evenicle                        |
| LOVESICK PUPPIES - 我们为了恋爱而诞生 -                         | lovesickpuppies                 |
| 魔女的夜宴                                                      | sothewitch                      |
| 花色七芒星                                                      | hanairo                         |
| 超电激冲锋卫士                                                  | chou_stryker                    |
| Bunny Black 2                                                   | bunnyblack2                     |
| ももいろ性癖開放宣言                                            | momose                          |
| 胸部战争～巨乳王国 VS 贫乳王国～                                | BoobWars2                       |
| 初恋前线。                                                      | hatsukoizensen                  |
| フレラバ ～Friend to Lover～                                    | ftolover                        |
| 駄作～爱丽丝与克罗艾连结之日～                                  | dasaku_add                      |
| 初恋预报。                                                      | hatsukoiyohou                   |
| まり∽くり -marriage crimson-                                    | marikuri                        |
| こんそめ！～combination somebody～                              | Consomme                        |
| 公主大人是王女                                                  | princess                        |
| 少女神域∽少女天狱 -The Garden of Fifth Zoa-                     | shoujo_shiniki                  |
| 妹欲 DEAD 催眠                                                  | MaiyokuDeadSaimin               |
| 自闭妹妹的重生调教                                              | imoutoneet                      |
| 太过幼齿的娇妻                                                  | osanazuma                       |
| 灰色的乐园                                                      | grisaia3                        |
| Nekopara vol.2                                                  | nekopara2                       |
| Gore Screaming Show                                             | GoreScreamingShow               |
| 妻色少女                                                        | tsumac                          |
| 古色迷宫轮舞曲～HISTOIRE DE DESTIN～                            | furuiro                         |
| 天色幻想岛                                                      | AmairoIsleNauts                 |
| Clover Day’s                                                    | cloverdays                      |
| 迷途羊的果树园                                                  | strayed_sheep                   |
| 花鸟风月 ～坠入爱河的花园之姬～                                 | kachou                          |
| Remember 最爱的妻子在其他男人怀中微笑●另一种可能                | remember                        |
| PriministAr MiniFanDisc 枝那森千里＆駒萱野 Ver.                 | PriministArMiniFanDisc          |
| 初恋 1/1                                                        | hatsukoi                        |
| 対魔忍阿莎姬 完全版                                             | TNA_com_dl                      |
| 恋妻くずし                                                      | koitsuma                        |
| 纯白之夏                                                        | mashirosummer                   |
| 紫影的索纳尼尔～何等美丽的记忆～                                | sonanyl                         |
| 姦獄娼女～Slave Girl Breeding～                                 | slave_girl_breeding             |
| LOVELY×CATION2                                                  | lovelyxcation2                  |
| 君之余影静静地摇曳着                                            | kiminago                        |
| 雨的边缘                                                        | AmenoMarginal                   |
| 兰斯 03 重制版                                                  | rance03                         |
| 机甲轰鸣                                                        | dedenden                        |
| 她的圣域                                                        | kanojo_seiiki                   |
| 妹的圣域                                                        | imouto_seiiki                   |
| 圆交少女～田径部悠希篇～                                        | enko_yukki                      |
| 魔法使之夜                                                      | mahoyo                          |
| 夏雪～summer_snow～                                             | natsuyuki                       |
| ラブ es エム                                                    | lovesm                          |
| フリフレ 2(Free Friends2)                                       | freefriends2                    |
| 妹 - 锁 -                                                       | Immobilizer                     |
| 対魔忍阿莎姬外伝                                                | TNA_g                           |
| Noble☆Works                                                     | NobleWorks                      |
| VenusBlood -GAIA-                                               | VenusBloodGAIA                  |
| 你朱眸里的五彩世界／映入红瞳的世界                              | shinku                          |
| 恋花绽放樱飞时                                                  | koisakura                       |
| Nekopara Vol.0                                                  | nekopara0                       |
| 巢作之龙                                                        | dragon                          |
| 女装海峡                                                        | josou_kaikyou                   |
| 晓之护卫～罪孽深重的末世论～                                    | akatsuki3                       |
| 丧服萝莉紧缚奇谭 美少女性奴隶调教                               | mofuku                          |
| 大图书馆的牧羊人 -Dreaming Sheep-                               | daito_fd                        |
| 最喜欢哥哥了                                                    | anisuki                         |
| Holy Breaker!                                                   | holybreaker                     |
| MeltyMoment MiniFanDisk 堇 & 千惠美                             | MeltyMoment_FD2                 |
| 永不落幕的前奏诗                                                | yorino                          |
| 秘密游戏 2：反叛                                                | rebellions2                     |
| 行尸走肉横行的世界只有我不被袭击 vol.1                          | zombie1                         |
| 妻しぼり                                                        | tsumashibori                    |
| 雨恋                                                            | amekoi                          |
| 装甲恶鬼村正 邪念篇                                             | muramasa_fd                     |
| 1/2 summer                                                      | OneSideSummer                   |
| 魔法学院恋～小小的魔法使～                                      | MagicalTales                    |
| 坂上台风                                                        | sakahari                        |
| 星彩的共鸣                                                      | rezonansu                       |
| 魔法少女的哥哥～拙劣谎言和不良少年～                            | mahoani                         |
| DEVILS DEVEL CONCEPT                                            | DEVILS_DEVEL_CONCEPT            |
| 滥交女～我妹妹是 bitch bitch biiiitch                           | yarimanmusume                   |
| 恋爱家庭教师露露美★Coordinate!                                  | rurumicoorde                    |
| 扔掉黄书以后，兄长情况有异                                      | anioka                          |
| 书淫，或是已失去的梦之物语                                      | shoin                           |
| フレラバ ～Friend to Lover～ ミニファンディスク                 | friendlover_fd                  |
| 美少女万华镜 - 神明所创造的的少女们 -                           | biman3                          |
| 时钟机关的 Ley-line - 黄昏时的境界线 -                          | leyline1                        |
| 晴霁之后，定是菜花盛开的好天气                                  | haruno                          |
| 天鹅之歌・死挽歌                                                | SWAN_SONG                       |
| CROSS†CHANNEL 復刻版                                            | CROSSCHANNEL                    |
| 少年之国的少女爱丽丝                                            | Shounen_no_Kuni_no_Shoujo_Alice |
| 满载爱的口袋                                                    | pokekoi                         |
| 仓野家的双胞胎故事                                              | kurano                          |
| 近月少女的礼仪                                                  | tsukiniyorisou                  |
| WHITE ALBUM2（白色相簿 2）Mini After Story                      | WA2_mas                         |
| すぴぱら (SPPL) STORY ＃01 – Spring Has Come！                  | sppl1                           |
| 女装学园（妊）                                                  | gakunin                         |
| 樱花开了。                                                      | skski                           |
| Nekopara vol.1                                                  | nekopara1                       |
| 亲吻魔王与红茶                                                  | kissmao                         |
| 缤纷少女 colorful cure                                          | colorful_cure                   |
| 12 月的 EVE／无尽的平安夜                                       | 12eve                           |
| 星辰恋曲的白色永恒                                              | WhiteEternity                   |
| フェノメノ Phenomeno                                            | Phenomeno                       |
| 装甲恶鬼村正                                                    | muramasa                        |
| 终末之际                                                        | shumatsu_ni_yosete              |
| 终极生化少女                                                    | weare                           |
| 智代 After                                                      | tomoyo_after                    |
| 真实之泪                                                        | truetears                       |
| 月姬 plus+disc                                                  | tsukihimeplusdisc               |
| 缘之空                                                          | yosuga                          |
| 幼驯染成了大总统                                                | osana                           |
| 游魂 -Kiss on my Deity-                                         | tayutama                        |
| 悠之空                                                          | haruka                          |
| 硬要无视与你的未来                                              | aete                            |
| 樱吹雪～千年之恋～                                              | hanafubuki                      |
| 寻找失去的未来                                                  | ushinawareta-mirai              |
| 虚之少女                                                        | kara_no_shoujo2                 |
| 星空的记忆 Eternal Heart                                        | hoshimemo_EH                    |
| 晓之护卫～主角们的休息日～                                      | akatsuki2                       |
| 星空的记忆 -Wish upon a shooting star-                          | hoshimemo                       |
| 晓之护卫                                                        | akatsuki                        |
| 小萝莉牛奶帕菲                                                  | kodomo                          |
| 向日葵的教会与长长的暑假                                        | himanatsu                       |
| 夏之雨                                                          | natsunoame                      |
| 夏空的英仙座                                                    | perseus                         |
| 夏空彼方                                                        | natsukana                       |
| 隙间樱花与谎言都市                                              | sukima                          |
| 舞 - HIME 命运系统树 修罗                                       | maihime                         |
| 五彩斑斓的曙光                                                  | irohika                         |
| 五彩斑斓的世界                                                  | irosekai                        |
| 屋上的百合灵                                                    | yurirei                         |
| 未来乡愁                                                        | miranosu                        |
| 为与明日君相逢                                                  | asukimi                         |
| 突然之间发现我已恋上你                                          | ikikoi                          |
| 天使的双枪                                                      | tenshinonityoukenjyuu           |
| 天神乱漫                                                        | tenshin                         |
| 特别婚                                                          | mechacon                        |
| 她们的流仪                                                      | kanojotachinoryuugi             |
| 四叶草                                                          | yotsunoha                       |
| 死神之吻乃离别之味                                              | sinikisu                        |
| 水夏 A.S+                                                       | suika_ASPLUS                    |
| 实妹相伴的大泉君                                                | realsister                      |
| 盛开的钢琴之森下                                                | pinonono                        |
| 少女与世界与点心之剑 ～Route of ICHIGO 1～                      | gwss3                           |
| 少女与世界与点心之剑 ～Route of NANA～                          | gwss2                           |
| 少女与世界与点心之剑 ～Route of AYANO～                         | gwss1                           |
| 如月金星                                                        | kisaragi                        |
| 染成茜色的坂道                                                  | akane                           |
| 青空下的约定                                                    | aozora                          |
| 七之不思议终结之时                                              | nanatoki                        |
| 樱色之云＊绯色之恋                                              | scarlet                         |
| Unless Terminalia                                               | UnlessTerminalia                |
| AMBITIOUS MISSION                                               | AMBITIOUSMISSION                |
| 时钟机关的 Ley-line - 阳炎中彷徨的魔女                          | leyline4                        |
| 柯罗的怀表                                                      | chronoclock                     |
| 星之少女与六华的姐妹                                            | hoshioto                        |
| 现在立刻就想对哥哥说我是妹妹                                    | imaimo                          |
| Golden Marriage                                                 | GoldenMarriage                  |
| 七色星露                                                        | nanatsu                         |
| 片羽                                                            | katahane                        |
| 女装山脉                                                        | 3myaku                          |
| 你和我与伊甸园的苹果                                            | eden_ringo                      |
| 命运石之门                                                      | STEINSGATE                      |
| 明日的世界                                                      | soshite                         |
| 妹调教日记√happy end & another                                  | imoutoFD                        |
| 妹调教日记                                                      | imouto                          |
| 美少女万华镜 - 献给曾经是少女的你 -                             | biman2.5                        |
| 美少女万华镜 - 勿忘草与永远的少女 -                             | biman2                          |
| 美少女万华镜 -被诅咒之传说少女 -                                | biman1                          |
| 美好的每一天 ～不连续的存在～                                   | suba                            |
| 恋爱 0 公里                                                     | renzero                         |
| 库特 wafter                                                     | kud_wafter                      |
| 壳之少女                                                        | kara_no_shoujo                  |
| 君与彼女与彼女之恋                                              | totono                          |
| 绝顶性器大发明                                                  | zettyou                         |
| 九十九之月                                                      | tsukumo                         |
| 架向星空之桥                                                    | hoshi                           |
| 秽翼的尤斯蒂娅                                                  | eustia                          |
| 黄雷的伽克苏恩～何等闪耀的勇气～                                | gahkthun                        |
| 黄昏的禁忌之药                                                  | sinsemilla                      |
| 红线                                                            | akaiito                         |
| 赫炎的印加诺克 ～何等美好的明日～ 全语音重生版                  | inganock                        |
| 鬼歌                                                            | oniuta                          |
| 鬼的捉迷藏 FD                                                   | oni_gokko_FD                    |
| 鬼的捉迷藏                                                      | oni_gokko                       |
| 夜明前的琉璃色 -Moonlight Cradle-                               | moonlightcradleFD               |
| 夜明前的琉璃色                                                  | moonlightcradle                 |
| 大图书馆的牧羊人 ～课馀的尾巴时光～                             | daito_fd2                       |
| 大图书馆的牧羊人                                                | daito                           |
| 从晴朗的朝色泛起之际开始                                        | asairo                          |
| 纯白交响曲                                                      | mashiroiro                      |
| 初雪樱                                                          | hatuyuki                        |
| 超级糖果 王道自有王道的理由                                     | shupuremu                       |
| 不要践踏天使的羽毛                                              | afeather                        |
| フリフレ Free Friends                                           | freefriend                      |
| your diary                                                      | yourdiary                       |
| 白色相簿 2                                                      | whitealbum2                     |
| 白色相簿 重制版                                                 | whitealbum                      |
| Tiny Dungeon ~BRAVE or SLAVE~                                   | TinyDungeon4                    |
| Tiny Dungeon ~BIRTH for YOURS~                                  | TinyDungeon3                    |
| Tiny Dungeon ~BLESS of DRAGON~                                  | TinyDungeon2                    |
| Tiny Dungeon ~BLACK and WHITE~                                  | TinyDungeon1                    |
| Strawberry Nauts                                                | StrawberryNauts                 |
| Stellar☆Theater                                                 | StellarTheater                  |
| SISTERS～夏の最后の日～ Ultra Edition                           | sisters                         |
| Shuffle                                                         | shuffle                         |
| SHINY DAYS                                                      | shinydays                       |
| SEX 战争 ~有爱的爱爱是禁止的哦~                                 | sexwar                          |
| Secret Game -KILLER QUEEN-                                      | SecretGame                      |
| School Days HQ                                                  | SchoolDays                      |
| Scarlett 日常的境界线                                           | scarlett                        |
| SAKURA~雪月华~                                                  | sakura                          |
| Rewrite Harvest festa！                                         | rewrite_HF                      |
| Rewrite                                                         | rewrite                         |
| Remember11                                                      | remember11                      |
| Really？Really！                                                | reallyreally                    |
| Quartett!                                                       | quartett                        |
| Phantom -PHANTOM OF INFERNO-                                    | phantom                         |
| PARA-SOL                                                        | para-sol                        |
| ONE                                                             | one                             |
| Never7                                                          | Never7                          |
| LOVELY×CATION                                                   | lovelycation                    |
| LOVELY QUEST                                                    | lovelyquest                     |
| KIRA☆KIRA 煌煌舞台                                              | kirakira                        |
| H2O～FOOTPRINTS IN THE SAND～                                   | h2o                             |
| G 弦上的魔王                                                    | gstring                         |
| Gift                                                            | gift                            |
| Flyable Heart                                                   | flyableheart                    |
| Fate/hollow ataraxia                                            | fateFD                          |
| Fate/Stay Night                                                 | fate                            |
| Ever17                                                          | ever17                          |
| euphoria HD Remaster                                            | euphoria                        |
| DRACU-RIOT!                                                     | dracuriot                       |
| D.C P.C.                                                        | DC1                             |
| D.C.II P.C.                                                     | DC2                             |
| D.C.II To You                                                   | DC2TY                           |
| D.C.II S.C.                                                     | DC2SC                           |
| Cure Girl                                                       | CuteGirl                        |
| CLANNAD \[Full Voice\]                                          | CLANNAD                         |
| CHAOS;HEAD（混沌之脑/混沌头脑）                                 | chaoshead                       |
| BALDR SKY DiveX DREAM WORLD                                     | baldrskyDX                      |
| Aster                                                           | aster                           |
| 3days                                                           | 3days                           |
| 七音学园 - 旅行部                                               | nanaganetravel                  |
| Princess × Princess                                             | princesssprincess               |
| 雪色暗号                                                        | yukiirosign                     |
| 夏之终熄                                                        | natsu_no_owari                  |
| Happy Live Show Up!                                             | happyliveshowup                 |
| 拿破仑少女  字典里没有不可能三个字的少女                        | napoleonmaiden                  |
| 龙姬混~日子 3                                                   | drapri3                         |
| 龙姬混~日子 2                                                   | drapri2                         |
| 龙姬混日子 LOVE＋PLUS                                           | drapri1fd                       |
| 龙姬混日子                                                      | drapri1                         |
| 恋爱成双（劈腿之恋）                                            | futamata                        |
| 恋爱成双 AS1                                                    | futamatafd1                     |
| 恋爱成双 AS2                                                    | futamatafd2                     |
| AMBITIOUS MISSION                                               | ambitious_mission               |
| 星之终途                                                        | stella                          |
| 献给神明般的你 Extended Edition                                 | kamikimi                        |
| FLIP＊FLOP ～INNOCENCE OVERCLOCK～                              | flipflopio                      |
| 青夏轨迹                                                        | aonatsu                         |
| 恋心与魔法誓言                                                  | koikoro                         |
| 天津罪                                                          | amatsutsumi                     |
| 恋心如花悄绚烂                                                  | koihana                         |
| 印刻天际的 Parallelogram                                        | sora_ni_kizanda_parallelogram   |
| 恋心如花悄绚烂～二人永相伴                                      | koihanafd                       |
| 玉响未来                                                        | tamayura                        |
| 未来收音机与人工鸽                                              | futureradio                     |
| With Ribbon                                                     | withribbon                      |
| 安娜贝尔女仆花园                                                | anabel                          |
| 交汇协奏曲                                                      | CrossConcerto                   |
| 兽娘道 ☆ Girlish Square                                         | kemonomichi                     |
| 真愿朦幻馆〜在时间暂停的洋馆里追寻明天的羔羊们                  | yumahorome                      |
| 恋爱 X 决胜战                                                   | renairoyale                     |
| 恋爱×决胜战 乃乃香&莲菜&由奈小故事                              | renairoyalefd1                  |
| 恋爱 X 决胜战 茉莉&汐音&苍 小故事                               | renairoyalefd2                  |
| Secret Agent〜骑校忍者物语〜                                    | secretagent                     |
| 回忆录                                                          | reminiscence                    |
| FLOWERS 冬篇                                                    | flower_winter                   |
| SPIRAL!!                                                        | spiral                          |
| 保健室的老师与哥特萝莉的校医                                    | hokeloli                        |
| 执事选择公主之时                                                | situaru                         |
| 恋剑乙女                                                        | koiken                          |
| 恋剑乙女 再燃                                                   | koikenfd                        |
| Dreamin' Her -我梦见了她                                        | dreaminher                      |
| 魔卡魅恋                                                        | magicha                         |
| 放课后的灰姑娘女孩                                              | HoukagoCinderella               |
| 放学后的灰姑娘 ～与你所走的最后一次放学路                       | HoukagoCinderellaFD             |
| 闪耀青春追逐记                                                  | kakenuke                        |
| LOVEPOTION SIXTYNINE                                            | love69                          |
| 五色浮影绽放于花之海洋                                          | hananono                        |
| ふゆから、くるる                                                | fuyukuru                        |
| 隐名之人（中の人などいない）                                    | nakanohito                      |
| 同级生 Remake                                                   | doukyuusei                      |
| 候鸟的梦                                                        | pieces                          |
| 毫无经验的同班同学π                                             | keikenzero                      |
| 家喵二三事 Vol. 1                                               | NekomimiSweetHousemates         |
| 无人知晓的天体之泪 本篇 第一卷                                  | daresora1                       |
| 无人知晓的天体之泪 本篇 第二卷                                  | daresora2                       |
| 无人知晓的天体之泪 本篇 第三卷                                  | daresora3                       |
| COSPLAY LOVE! : Enchanted princess                              | cosplaylove                     |
| 红月摇曳的恋之星火                                              | yureaka                         |
| 与红叶同在来自盛夏的某日                                        | yureakafd1                      |
| 与旭同在来自盛夏的某日                                          | yureakafd2                      |
| 奇异恩典·圣夜的小镇                                             | amegure                         |
| 七色的轮回转生                                                  | nanairo                         |
| U-ena -空焰火少女                                               | u-ena                           |
| Robotics;Notes ELITE                                            | roboticsnotes                   |
| 心之形 心之色 心之声                                            | kokoro                          |
| 我的恋天使太废柴了好可怕（我的恋天使太一无是处了好担心）        | cupid                           |
| LUNARiA Virtualized Moonchild                                   | lunaria                         |
| 丑小鸭与文字祸                                                  | mojika                          |
| へんこい 変恋≒黒歴史                                            | henkoi                          |
| 保健室的老师与沉迷吹泡泡的助手                                  | hokejyo                         |
| 逆转爱恋                                                        | atchikoi                        |
| 雪境迷途遇仙踪                                                  | madokino                        |
| 神之国的魔法使                                                  | kamimahou                       |
| Dal Segno                                                       | dalsegno                        |
| 淑女同萌！-Superior Entelecheia                                 | hellolady                       |
| NinNinDays                                                      | ninnindays                      |
| 猫神大人与七颗星星                                              | nekotsubo                       |
| 同居女友                                                        | aibeya                          |
| 我家大小姐不懂妖怪的常识                                        | youjou                          |
| 正负战争                                                        | puramaiwars                     |
| 灰色的果实                                                      | grisaia                         |
| 灰色的迷宫                                                      | grisaia2                        |
| 不可视之药与坎坷的命运                                          | fukasuki                        |
| LILYCLE 彩虹舞台                                                | lyrs                            |
| 湛蓝牢笼                                                        | railofmobius                    |
| 爱之钥 3                                                        | aikagi3                         |
| 人气声优的养成方式                                              | ninkiseiyuu                     |
| Sugar＊Style                                                    | sugarstyle                      |
| Sugar＊Style Music and Happiness Pack                           | sugarstyleFD                    |
| 夏末花开                                                        | flowersblooming                 |
| 终端少女                                                        | terminalgirls                   |
| Primal Hearts2                                                  | primalhearts2                   |
| 一生推不如一生恋                                                | oshirabu                        |
| 一生推不如一生恋 love or die                                    | oshirabuFD                      |
| 小狗新娘～欢迎光临尾巴摇摇～                                    | wanko                           |
| 汪酱出嫁～新二尾嫁到～                                          | wanko2                          |
| 想谈个恋爱吗？那就给钱吧／恋爱，我就借走了                      | koikari                         |
| 想谈个恋爱吗？那就给钱吧绘未＆八纯 迷你小剧场                   | koikariFD1                      |
| 想谈个恋爱吗？那就给钱吧~椿&千夏篇 迷你小剧场                   | koikariFD2                      |
| 神社里的猫巫女                                                  | nekomiko                        |
| 时廻者                                                          | loopers                         |
| 冥契的牧神节                                                    | lupercalia                      |
| 她与我的眼镜事件〜 伊波乙叶篇                                   | megasuki1                       |
| 她与我的眼镜事件〜 伊波乙叶篇                                   | megasuki2                       |
| BlackHairGirl                                                   | blackhairgirl                   |
| 天之少女                                                        | kara_no_shoujo3                 |
| 星空列车与白的旅行                                              | hoshitetsu                      |
| 在月之彼岸邂逅~甜蜜的夏日彩虹~                                  | tsukikanaFD                     |
| Island Diary                                                    | islanddiary                     |
| 猫咪相随★恋樱纷飞                                               | nekotsuku                       |
| 白诘戒指~四等分的花嫁 全是我的~                                 | shirotsume                      |
| Berry's                                                         | berrys                          |
| 她与我的眼镜事件〜 星野优羽姬篇                                 | megasuki3                       |
| 鲸神                                                            | kujira                          |
| 啾啾爱恋！                                                      | hiyokostrike                    |
| 甜蜜恶魔！Honey＆Devil                                          | hanidevi                        |
| 工口龙卷风！～偶像妹妹太色情了，我的理性即将狂风大作！？        | eroimo1                         |
| 工口龙卷风！2～工口旋涡以妹妹为中心急速扩大中！～               | eroimo2                         |
| 工口龙卷风！3～和娇嫩欲滴的妹妹们一起前往情欲满满的温泉旅馆！～ | eroimo3                         |
| PRETTY×CATION2                                                  | prettyxcation2                  |
| PURELY×C∧TION                                                   | prettyxaction                   |
| 流星钟摆之心                                                    | pendulumheart                   |
| 妄想 Complete！                                                 | mcom                            |
| 水莲与紫苑                                                      | sts                             |
| 恋爱教室                                                        | renaikyoushitu                  |
| 恋爱与魔法与管理人～恋爱连锁篇～                                | koikan                          |
| 哥哥，还没做好 KISS 的准备吗？                                  | onikiss                         |
| 离水平线还有多远？- 深蓝的天空和洁白的翅膀 -                    | suiheisen                       |
| 哥哥，还没做好 KISS 的准备吗？还没做好 H 的准备吗？             | onikissh                        |
| 星辰恋曲的白色永恒-Finale                                       | WhiteEternalFinale              |
| 罪恋×2／3                                                       | tsumikoi                        |
| E 学院生活（E 学园生活）                                        | eschoollife                     |
| 水葬银货                                                        | suisou_ginka_no_istoria         |
| 新娘太好找了我很方！                                            | yomeyaba                        |
| Nekopara vol.4                                                  | nekopara4                       |
| Nekopara vol.3                                                  | nekopara3                       |
| 雪恋交融                                                        | yukikoimelt                     |
| 苍之彼方的四重奏 EXTRA1                                         | aokanaextra1                    |
| 苍之彼方的四重奏 EXTRA2                                         | aokanaextra2                    |
| 祝姬                                                            | iwaihime                        |
| 恋爱中的她的笨拙舞台                                            | koikana                         |
| 尚有佳蜜伴恋心                                                  | koiama                          |
| 尚有佳蜜伴恋心 2                                                | koiama2                         |
| 恋色空模样                                                      | koisora                         |
| 吃醋大作战（醋意乱流）                                          | yakimochi                       |
| 四季的谎言-Complete Four Seasons-                               | fulluso                         |
| 白刃闪耀的恋之旋律                                              | mekurabe                        |
| 恋爱定位❤神社×前辈                                              | localove3                       |
| 恋爱定位❤电车×同级生                                            | localove2                       |
| 恋爱定位❤同居×后辈                                              | localove1                       |
| 11 月的理想乡                                                   | 11gatsu                         |
| 树莓立方体                                                      | raspberrycube                   |
| 魔女的花园                                                      | witchsgarden                    |
| 尽情撒娇，请更多的依赖我吧，哥哥！                              | oniama                          |
| 金色 Loveriche -Golden Time                                     | kinkoi_fd                       |
| 爱莉娅嘉年华                                                    | aliascarnival                   |
| 爱莉娅嘉年华 FD-花绽晴空                                        | aliascarnival_fd                |
| 春开，意遥遥。                                                  | harumadekururu                  |
| 美少女万华镜 理与迷宫的少女                                     | biman5                          |
| 密语                                                            | aikotoba                        |
| 缘染此叶、化作恋红                                              | yorikure                        |
| Animal☆Panic                                                    | anipani                         |
| Primal Hearts                                                   | primalhearts                    |
| 与你心相连                                                      | purexconnect                    |
| 幸福噩梦 FD                                                     | hapymaherfd                     |
| 9-nine-雪色雪花雪余痕                                           | nine_yukiiro                    |
| Happiness！2 樱花盛典                                           | happiness2                      |
| I/O revisionII                                                  | io_r2                           |
| 樱之杜†净梦者 2                                                 | sakuranomori2                   |
| 命运交织的塔罗牌                                                | tarot                           |
| 魔卡魅恋！零之编年史                                            | magicha_trial                   |
| 茜色的境界线                                                    | kanesen                         |
| 由梦想与色彩编织而成                                            | yumeiro                         |
| 忘却管家与恋爱大小姐的回忆录                                    | koimemo                         |
| Re：LieF ～给挚爱的你～                                         | relief                          |
| 间接之恋                                                        | hitoren                         |
| 樱之诗                                                          | sakuuta                         |
| 三千心世界，梦终将实现                                          | yurauka                         |
| 恋爱少女人格崩坏                                                | charabure                       |
| 同一屋簷下，羽翼下的故事                                        | hitotsuba                       |
| 抬头看看吧，看那天上的繁星 FINE DAYS                            | FullOfStarFD1                   |
| 仰望夜空星辰 Interstellar Focus                                 | FullOfStarFD2                   |
| 梦现 Re:Master                                                  | yurimaster                      |
| 梦现 Re:After                                                   | yuriafter                       |
| 箱庭逻辑                                                        | hakologic                       |
| 巧可甜恋                                                        | amachoco                        |
| 与兽耳娘共度乡村生活                                            | wabisabi                        |
| 重返蓝鲸岛                                                      | shironagasu                     |
| Study§Steady                                                    | ststeady                        |
| 碧海晴空、彼岸相连                                              | umikana                         |
| 有少女献鸣的爱之奏章 ～愿奏章之上思念满载～                     | otomelo_fd                      |
| 有少女献鸣的爱之奏章                                            | otomelo                         |
| 恋慕之心的交织方式                                              | koikata                         |
| 恋慕之心的交织方式 ～愿交织的思念永恒～                         | koikata_fd                      |
| 一如她的撒娇方式                                                | amanari                         |
| TrymenT ―献给渴望改变的你― AlphA 篇                             | tryment_alpha                   |
| 白恋 SAKURA＊GRAM                                               | shirogra                        |
| 少女谱写的爱的咏叹调                                            | otokana                         |
| 樱舞少女的圆舞曲                                                | sakuoto                         |
| 樱舞少女的圆舞曲~与你一起看冬樱~                                | sakuoto_fd                      |
| 追忆夏色年华                                                    | natsunoiro                      |
| 猫忍之心 3                                                      | nekonin3                        |
| 圣骑士 Melty☆Lovers                                             | seikishi_melty_lovers           |
| sorceress-aliv！                                                | sosaara                         |
| LOVE³ -Love Cube-                                               | lovecube                        |
| 约会大作战：凛绪轮回                                            | datealive                       |
| Deep One                                                        | deepone                         |
| 竜騎士 Bloody†Saga                                              | ryuukishi_bloody_saga           |
| 相见 5 分属于我！时间停止和不可避的命运                         | orefukahi                       |
| 在这苍穹展翅 - 飞行日志                                         | konoaobasa_fd                   |
| 在这苍穹展翅                                                    | konoaobasa                      |
| 夜晚，徘徊在我们的辅导教室                                      | yorubuku                        |
| 塔下兵势×异之课题                                               | tounosita                       |
| 兽娘育成方案                                                    | kemomusu                        |
| 牛顿与苹果树                                                    | newrin                          |
| 伊人天使妹侧畔                                                  | imoten                          |
| 茂伸奇谈                                                        | monobeno                        |
| 若能与你再次相见                                                | TL_su                           |
| 慕情之心今随君作伴                                              | tonakoi                         |
| 慕情之心今随君作伴 ～THE RESPECTIVE HAPPINESS～                 | tonakoi_fd                      |
| 日冕花开 vol 1                                                  | coroblo1                        |
| 日冕花开 vol 2                                                  | coroblo2                        |
| 日冕花开 vol 3                                                  | coroblo3                        |
| 恋神 - 神之舞台 -                                               | lovekami                        |
| 娇小少女的小夜曲                                                | chiikano                        |
| 神嫁~我的甜心女神                                               | yomegami                        |
| fault - milestone two sideabove                                 | faultms2                        |
| 与我恋爱的废柴恶魔 ～甜腻后日谈～                               | DitzyDemonsFD                   |
| 当孤僻之人遇上青春生活                                          | kuremono                        |
| 有少女涂彩的爱之芬芳                                            | otoiro                          |
| HAZEMAN -THE LOCAL HERO-（大地侠）                              | hazeman                         |
| 青春 x 好奇相伴的三角恋爱                                       | sukitosuki                      |
| 糖调！                                                          | syugaten                        |
| 挚爱随行 - 倾慕相伴                                             | sukisuki                        |
| 死亡竞赛恋爱喜剧                                                | DMLC                            |
| 四叶奇迹                                                        | clover_point                    |
| FLIP＊FLOP ～RAMBLING OVERRUN～                                 | flipflopran                     |
| 怀揣思念同我再会                                                | aikoi                           |
| 甘神大人的糖浆恋祭                                              | amakoi                          |
| 我的年假恋爱物语                                                | love_on_leave                   |
| 天籁人偶 冬空焰火／雪华纹样                                     | primadoll01                     |
| 无法忍耐的处男哥哥和不直率的叛逆妹妹                            | namaikisister                   |
| 飞鸟会长不肯认输！                                              | asunabi                         |
| 空恋                                                            | sorakoi                         |
| 巧克甜恋 2                                                      | amachoco2                       |
| 智以泪聚                                                        | ruitomo_fve                     |
| 悠久的钟声                                                      | UQcampanella                    |
| 天使☆纷扰 RE-BOOT!                                              | tenshi_sz                       |
| 波子汽水                                                        | lamune                          |
| 多愁善感死亡轮回                                                | sentimental_death_loop          |
| 与鲨鱼共度的七日间                                              | samenana                        |
| 创作彼女的恋爱方程式                                            | sousakukanojo                   |
| 与女大生的同居生活                                              | life_with_a_college_girl        |
| 与青梅竹马大小姐甜密性福的同居生活                              | ojyou                           |
| 百合色的 Frottage                                               | yuriiro_frottage                |
| 匿名代码                                                        | anonymous_code                  |
| 本所七大不可思议                                                | paranormasight                  |
| 炼爱秘仪                                                        | arcana_alchemia                 |
| 喜欢我的话就要说出来！                                          | sugar_spice_3                   |
| 我的眼睛能看光光，不可知的未来与透视的命运！                    | orefukachi                      |
| 提早绽放的黑百合                                                | hayazaki_no_kuroyuri            |
| GINKA                                                           | ginka                           |
| 娇蛮任性 HIGHSPEC OC                                            | wagahighOC                      |
| Liminal Border Part I                                           | liminal_border_1                |
| 兽娘道 ☆ Girlish Square LOVE+PLUS                               | kemonomichi_loveplus            |
| ghostpia                                                        | ghostpia                        |
| Hira Hira Hihiru                                                | hirahirahihiru                  |
| 流景之海的艾佩理雅                                              | apeiria                         |
| 间宫摩美想要疗愈你                                              | Mamiya_Mami_wa_Iyashite_Agetai  |
| 宿星 Girlfriend FD                                              | syukugar_fd                     |
| 兽娘道 ☆ Girlish Square 2                                       | kemonomichi2                    |
| 生死永相随永相伴                                                | stand_by_you                    |
| 八卦恋爱                                                        | koibana                         |
| Monkeys                                                         | monkeys                         |
| 女子宿舍的管理员                                                | garudoma                        |
| AQUA                                                            | aqua                            |
| 住在下体升级岛上贫乳该如何是好？                                | nukitashi                       |
| 住在下体升级岛上贫乳该如何是好？2                               | nukitashi2                      |
